from threading import Thread
import socket
import os
import signal
import json
import getopt
import sys
import protocols
from game import Game

id = 1


class ClientController(Thread):
    def __init__(self, client_socket, client_address, games, clients):
        Thread.__init__(self)
        self.client_socket = client_socket
        self.client_address = client_address
        self.end = False
        self.games = games
        self.clients = clients
        self.name = ""
        self.game = None
        self.player = None

    def control_join_message(self, message):
        self.name = message["name"]
        print("(WELCOME) {} joined the server".format(self.name))
        self.send_welcome_message()

    def send_welcome_message(self):
        menu = '''\t*****************************
        *   Welcome to the server:  *
        *   1.-Create game          *
        *   2.-Join game            *
        *   3.-Exit                 *
        *****************************'''
        options_range = [1, 2, 3]
        message = {"header": protocols.WELCOME, "menu": menu, "options range": options_range}
        protocols.send_one_message(self.client_socket, json.dumps(message).encode())

    def control_send_server_option_message(self, message):
        option = message["option"]
        number_of_players = message["number of players"]
        number_of_stages = message["number of stages"]
        if option == 1:
            global id
            print("(CREATE) {} created a game".format(self.name))
            self.game = Game(self.name, number_of_players, number_of_stages)
            self.games[id] = self.game
            self.clients[self.client_address] = id
            id += 1
            self.send_choose_character()

        elif option == 2:
            self.send_games()
        else:
            print("(EXIT) {} disconnected".format(self.name))
            reason = "{} disconnected.".format(self.name)
            message = {"header": protocols.SEND_DC_SERVER, "reason": reason}
            protocols.send_one_message(self.client_socket, json.dumps(message).encode())
            self.end = True

    def send_choose_character(self):
        menu = self.game.available_characters()
        options_range = [1, 2, 3, 4]
        message = {"header": protocols.CHOOSE_CHARACTER, "menu": menu, "options range": options_range}
        protocols.send_one_message(self.client_socket, json.dumps(message).encode())

    def available_games(self):
        games_list = ""
        for key, value in self.games.items():
            if not self.games[key].is_full():
                games_list += "---------------------------------------------------------------------------\n"
                games_list += "\t\tGame {} ({}'s game) - {}\n".format(key, self.games[key].starter_player,
                                                                      self.games[key].info())
                games_list += "---------------------------------------------------------------------------\n"
        if games_list == "":
            games_list += "There are no available games."
        return games_list

    def available_games_id(self):
        games_id = []
        for key, value in self.games.items():
            if not self.games[key].is_full():
                games_id.append(key)
        return games_id

    def send_games(self):
        available_games = self.available_games()
        games_id = self.available_games_id()
        message = {"header": protocols.SEND_GAMES, "games list": available_games, "games id": games_id}
        protocols.send_one_message(self.client_socket, json.dumps(message).encode())

    def send_dc_server(self):
        for player in self.game.alive_and_dead_players():
            reason = "{} disconnected. The game can not continue".format(self.name)
            message = {"header": protocols.SEND_DC_SERVER, "reason": reason}
            protocols.send_one_message(player.client_socket, json.dumps(message).encode())

    def control_send_character_message(self, message):
        option = message["option"]
        self.player = self.game.add_player(option, self.name, self.client_socket, self.client_address)
        if self.game.is_full():
            print("(START) {} started a game".format(self.game.starter_player))
            info = self.game.first_stage_info()
            message = {"header": protocols.SERVER_MSG, "message": info}
            for player in self.game.alive_and_dead_players():
                protocols.send_one_message(player.client_socket, json.dumps(message).encode())
            self.send_your_turn_message()
        else:
            msg = "You must wait for more players: {}".format(self.game.info())
            message = {"header": protocols.SERVER_MSG, "message": msg}
            for player in self.game.alive_and_dead_players():
                protocols.send_one_message(player.client_socket, json.dumps(message).encode())

    def send_your_turn_message(self):
        current_player = self.game.current_player_turn()
        msg = self.game.current_player_turn_info()
        options_range = ['a', 's']
        try:
            message = {"header": protocols.YOUR_TURN, "message": msg, "options range": options_range}
            protocols.send_one_message(current_player.client_socket, json.dumps(message).encode())
        except BrokenPipeError:
            print("Client disconnected")

    def control_send_character_command(self, message):
        command = message["command"]
        action = self.game.player_action(self.player, command)
        for player in self.game.alive_and_dead_players():
            try:
                message = {"header": protocols.SERVER_MSG, "message": action}
                protocols.send_one_message(player.client_socket, json.dumps(message).encode())
            except BrokenPipeError:
                print("Client disconnected")
        if self.game.game_finished():
            self.send_end_game_message()
        else:
            self.send_your_turn_message()

    def send_end_game_message(self):
        win = None
        if self.game.all_enemies_are_dead():
            win = True
        elif self.game.all_players_are_dead():
            win = False
        for player in self.game.alive_and_dead_players():
            message = {"header": protocols.SEND_END_GAME, "win": win}
            protocols.send_one_message(player.client_socket, json.dumps(message).encode())

    def control_send_game_choice_message(self, message):
        game_id = message["game id"]
        if game_id in self.games:
            if self.games[game_id].is_full():
                joined = False
                message = {"header": protocols.SEND_VALID_GAME, "joined": joined}
                protocols.send_one_message(self.client_socket, json.dumps(message).encode())
            else:
                self.clients[self.client_address] = game_id
                self.game = self.games[game_id]
                print("(JOIN) {} joined the game".format(self.name))
                joined = True
                message = {"header": protocols.SEND_VALID_GAME, "joined": joined}
                protocols.send_one_message(self.client_socket, json.dumps(message).encode())
                self.send_choose_character()
        else:
            msg = "This game does not exist."
            message = {"header": protocols.SERVER_MSG, "message": msg}
            protocols.send_one_message(self.client_socket, json.dumps(message).encode())
            self.send_games()

    def control_send_dc_me(self):
        self.game.remove_player(self.player)
        for player in self.game.alive_and_dead_players():
            reason = "Player disconnected. The game can not continue"
            message = {"header": protocols.SEND_DC_SERVER, "reason": reason}
            protocols.send_one_message(player.client_socket, json.dumps(message).encode())

    def control_message(self, message):
        header = message["header"]
        print("{} message received".format(header))
        if header == protocols.JOIN:
            self.control_join_message(message)
        elif header == protocols.SEND_SERVER_OPTION:
            self.control_send_server_option_message(message)
        elif header == protocols.SEND_CHARACTER:
            self.control_send_character_message(message)
        elif header == protocols.SEND_CHARACTER_COMMAND:
            self.control_send_character_command(message)
        elif header == protocols.SEND_GAME_CHOICE:
            self.control_send_game_choice_message(message)
        elif header == protocols.SEND_DC_ME:
            self.control_send_dc_me()
        else:
            print("ERROR: Invalid message received")

    def run(self):
        ip, port = self.client_address
        print("Connection established with {}:{}".format(ip, port))
        while not self.end:
            try:
                buffer = protocols.recv_one_message(self.client_socket)
                message = json.loads(buffer.decode())
                self.control_message(message)
            except TypeError:
                self.end = True
                print("Client disconnected")
            except AttributeError:
                self.end = True
                print("Client disconnected")
        self.client_socket.close()


class Server(Thread):
    DEFAULT_PORT = 8080

    def __init__(self, port):
        Thread.__init__(self)
        self.my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.my_socket.bind((socket.gethostname(), port))
        self.my_socket.listen()
        self.games = {}
        self.clients = {}

    def run(self):
        ip, port = self.my_socket.getsockname()
        print("Server started at {}:{}".format(ip, port))
        while True:
            client_socket, client_address = self.my_socket.accept()
            client_controller = ClientController(client_socket, client_address, self.games, self.clients)
            client_controller.start()


class ArgsError(Exception):
    def __init__(self, msg):
        super().__init__("Error: {}".format(msg))


def parse_args():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "o:", "port=")
    except getopt.GetoptError:
        raise ArgsError("You enter wrong arguments. Finishing program")

    port_address = Server.DEFAULT_PORT
    try:
        for o, a in opts:
            if o in ("-o", "--port"):
                port_address = int(a)
    except ValueError:
        raise ArgsError("Type of arguments is wrong. Finishing program")
    return port_address


if __name__ == '__main__':
    pid = os.getpid()
    try:
        port = parse_args()
        server = Server(port)
        server.start()
        input("Press enter to exit")
        print("Server closed by admin")
        os.kill(pid, signal.SIGKILL)
    except ArgsError as err:
        print(err)
    except KeyboardInterrupt:
        print("Server closed by CTRL+C")
        os.kill(pid, signal.SIGTERM)
